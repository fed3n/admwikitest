# Firewall configuraton

La configurazione del firewall a protezione del server è essenziale per la messa in opera di una nuova macchina.
Lo strumento che si utilizza in questo contesto è **_IPTABLES_**.

Iptables è un programma di utilità dello spazio utente che consente a un amministratore di sistema di configurare le regole di filtro dei pacchetti IP del firewall del kernel Linux, implementate come diversi moduli Netfilter.

Iptables però ha la peculiarità che ad ogni riavvio del sistema le regole, precedentemente inserite, vengono cencellate e se vogliamo continuare a proteggere il sistema è necessario reinserirle.

Ovviamente se si è reso necessario un riavvio del sistema non andremo a reinserirle manualmente, esistono infatti svariati metodi di riconfigurazione dei filtri di iptables.

Fra i più utilizzati citiamo:
<ol>
<li> Il pacchetto iptables-persistent</li>
Creando un file.sh con la serie di istruzioni per il settaggio delle regole possiamo usare:
<li> Una regola in Cron</li>
<li> Aggiungere il nostro file.sh all'interno della cartella /etc/init.d/file.sh </li>
<li> Creare un service di systemd</li>
</ol>

Noi in questo caso procederemo utilizzando il quarto metodo.

## Creazione e configurazione del servizio

Creiamo una directory /etc/iptables e ci inseriamo il nostro file iptables-set.sh con le istruzioni di configurazione di **Iptables**.

Creiamo un nuovo file in **/etc/systemd/system/** lo chiamiamo per comodità **_iptables-run.service_** e lo editiamo come segue:

```
[Unit]
Description=iptables-run service

[Service]
ExecStart=/etc/iptables/iptables-set.sh

[Install]
WantedBy=multi-user.target
```

Assicuriamoci che il nostro script in /etc/iptables/iptables-set.sh sia eseguibile:

```chmod u+x /etc/iptables/iptables-set.sh ```

Come ultimo passaggio abilitiamo il suo avvio al Boot del sistema:

```sudo systemctl enable iptables-run```

### Note:

Non è necessario utilizzare **_sudo_** all'interno del nostro service, poichè di default gli **_user service_** vengono eseguiti come root.