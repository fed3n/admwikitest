# AdmStaff Infrastructure


L'infrastruttura dell'AdmStaff è pensata per essere facilmente ricorstruibile e mantenibile.
Tra i servizi troviamo:

- DNS
- LDAP
- MAIL
- HUGO website

L'intera infrastruttura è pensata per essere mantenuta tramite il processo chiamato Infrastructure as Code (IaC).
Ogni servizio viene gestito tramite files di definizione leggibili dalla macchina che eseguiti da un gestore di macchine virtuali, come ad esempio Vagrant, fa si che vengano configurati automaticamente: installando dipendenze, configurando le schede di rete, copiando file di configurazione all'interno delle macchine virtuali e preparando l'ambiente virtuale sotto ogni aspetto.

Nel file di configurazione  *Vagrantfile* troviamo il set di istruzioni che **Vagrant** eseguirà per configurare l'hardware assegnato ad ogni singola macchina virtuale e dove viene assegnato anche l'indirizzo IP statico della macchina.

I files con i set di istruzioni che il gestore va ad eseguire per preparare le macchine sono scritti in **Ansible**.
Ansible è il sistema che ci permette di semplificare radicalmente l'automazione del processo di mantenimento dei sistemi.

La struttura utilizzata per la creazione della rete è basata sui *ROLES*, che permettono di caricare automaticamente tasks ,handlers, vars_files e altri artefatti Ansible based.
Una volta che vengono raggruppate le informazioni necessarie per la configurazione possono essere facilmente riutilizzate e condivise con altri utenti.
Di seguito un esempio di struttura di un progetto:
```	
site.yml
webservers.yml
fooservers.yml
roles/
   common/
     files/
     templates/
     tasks/
     handlers/
     vars/
     defaults/
     meta/
   webservers/
     files/
     templates/
     tasks/
     handlers/
     vars/
     defaults/
     meta/
```
Dentro ogni directory andremo ad inserire un file main.yml dove inseriremo i necessari alla configurazione.
Il playbook webserver.yml sarà così creato:
```
---
- hosts: webservers
  roles:
     - common
     - webservers
```
Nella macchina host: webserver verranno eseguiti i task presenti in common/ e webserver/.
La directory webserver/ poteva essere chiamata a piacere, in base alle esigenze si poteva nominare ad esempio apache2/.
Ogni playbook.yml presente può richiamare qualsiasi *roles*.

Se avessimo dovuto installare apache ad esempio avremmo creato i seguenti file e directory:
```
roles/
	tasks/main.yml
	vars/main.yml
	handler/main.yml
	files/main.yml
```
In tasks/main.yml inseriremo le task da eseguire:
```
---
# tasks file for web
 - name: Install apache2
   become: True
   package:
     update_cache: True
     name: [ "apache2" ]
     state: present

# ....

 - name: Configure iptables for HTTP / HTTPS
   become: True
   iptables:
     chain: INPUT
     protocol: tcp
     ctstate: NEW
     destination_port: "{{ item }}"
     jump: ACCEPT
     action: append
   loop:
     - 80
     - 443

```
- In vars/main.yml le definizioni delle variabili utilizzate in tasks/main.yml.
- In files/ metteremo gli evetuali file di configurazione o fisicamente i file che avvierà il server web, che saranno copiati dentro la macchina.
- in handler/main.yml metteremo il comando di riavvio del servizio.
Per maggiori informazioni sulle specifiche delle varie directory controllare la documentazione ufficiale sui [**Roles**](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html) di Ansible.














