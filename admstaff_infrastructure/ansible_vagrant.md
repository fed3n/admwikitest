# IaC (infrastructure as a Code)

Talvolta definita "infrastruttura programmabile", Infrastructure-as-Code (IaC) tratta la configurazione dell’infrastruttura esattamente come se fosse software di programmazione. Si tratta di un elemento fondante del cloud computing che è essenziale per DevOps.

In pratica è il metodo con cui gli amministratori di sistema distribuiscono i sistemi virtuali tramite file di configurazione.
In passato ad ogni nuova richiesta di messa in servizio di una nuova macchina ne conseguiva la sua, necessaria, configurazione.
Oggi, in aggiunta al maggiore utilizzo di macchine virtuali e container, non è più necessario utilizzare script o eseguire configurazioni manualmente.
Tramite file di configurazione è possibile delegare ed automatizzare il procedimento di messa in opera della macchina. 

### **Vantaggi**
Infrastructure-as-Code  consente di gestire le macchine virtuali come se fossero programmi, senza la necessità di configurare e aggiornare manualmente i singoli elementi dell’hardware, rendendo l’infrastruttura estremamente “elastica”, vale a dire ripetibile e scalabile: un operatore può distribuire e gestire una o mille macchine usando lo stesso set di codice. Velocità, risparmio e riduzione dei rischi sono i vantaggi immediati di Infrastructure-as-Code.

### **In che modo Iac si rapporta a DevOps?**
Il concetto di IaC è il quadro in cui si inserisce la nascita di DevOps. Un confine sempre più indistinto tra il codice con cui vengono eseguite le applicazioni e quello che configura le infrastrutture comporta un confine altrettanto indistinto tra i compiti di sviluppatori e responsabili delle operazioni.

### **In che modo Iac si rapporta a IaaS?**
Infrastructure-as-Code supporta IaaS (infrastructure as a Service) in quanto consente di vendere come pacchetto di servizi l’accesso a macchine virtuali e a  strumenti di gestione intuitivi basati su software.

![alt-text](images/IaC.webp "IaC")

### __Strumenti a disposizione:__

![alt-text](images/IaC_tools.png "Tools")

Nella nostra infrastruttura noi utilizziamo in Ansible in combinazione a Vagrant.

# __Vagrant__
Vagrant è un gestore di macchine virtuali open source; funziona con molti software di tipo hypervisor, denominati providers, tra cui **_VirtualBox, VMware e KVM_**.

Vagrant, insieme a Docker è uno dei principali software per il deployment portatile di ambienti di sviluppo di applicazioni.

Grazie a Vagrant infatti è possibile costruire delle macchine virtuali che utilizzano le stesse configurazioni e possono essere create, modificate e cancellate con facilità.

I componenti di Vagrant sono:
<ol>
<li>Providers</li>
<li>Box</li>
<li>Provisioning</li>
</ol>